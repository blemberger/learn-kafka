package org.blemberger.training.streamprocessor.model;

import twitter4j.Status;

import java.util.Date;

public class Tweet {

    private final Date created;
    private final String screenName;
    private final String text;


    public Tweet(Status statusTweet) {
        this.created = statusTweet.getCreatedAt();
        this.screenName =  statusTweet.getUser().getScreenName();
        this.text = statusTweet.getText();
    }

    public Date getCreated() {
        return created;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getText() {
        return text;
    }
}
