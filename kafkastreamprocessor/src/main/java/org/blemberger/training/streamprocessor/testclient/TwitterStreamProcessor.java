package org.blemberger.training.streamprocessor.testclient;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TwitterStreamProcessor {

    private static String consumerKey;
    private static String consumerSecret;
    private static String accessToken;
    private static String accessTokenSecret;

    public static void main(String[] args) {

        if (args.length == 4) {
            consumerKey = args[0];
            consumerSecret = args[1];
            accessToken = args[2];
            accessTokenSecret = args[3];
        }
        else {
            System.err.println("Need 4 arguments: consumerKey, consumerSecret, accessToken, accessTokenSecret");
            System.exit(-1);
        }

        /** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);

        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
        List<String> terms = Lists.newArrayList("trump", "impeachment");
        hosebirdEndpoint.trackTerms(terms);

        // These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, accessToken, accessTokenSecret);

        ClientBuilder builder = new ClientBuilder()
                                        .name("Hosebird-Client-01")                              // optional: mainly for the logs
                                        .hosts(hosebirdHosts)
                                        .authentication(hosebirdAuth)
                                        .endpoint(hosebirdEndpoint)
                                        .processor(new StringDelimitedProcessor(msgQueue));

        Client hosebirdClient = builder.build();
        // Attempts to establish a connection.
        hosebirdClient.connect();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // on a different thread, or multiple different threads....
                while (!hosebirdClient.isDone()) {
                    try {
                        String msg = msgQueue.take();
                        System.out.println(msg);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }
}