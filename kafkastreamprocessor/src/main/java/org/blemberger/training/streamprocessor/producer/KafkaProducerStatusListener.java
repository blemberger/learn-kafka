package org.blemberger.training.streamprocessor.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.blemberger.training.streamprocessor.model.Tweet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

public class KafkaProducerStatusListener implements StatusListener {

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducerStatusListener.class);

    private final KafkaProducer<Long, String> producer;
    private final String kafkaTopic;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public KafkaProducerStatusListener(KafkaProducer<Long, String> producer, String topic) {
        this.producer = producer;
        this.kafkaTopic = topic;
    }

    @Override
    public void onStatus(Status status) {
        logger.info("Status tweet on {} from: {}\nText: {}", status.getCreatedAt(), status.getUser().getScreenName(), status.getText());
        Tweet tweet = new Tweet(status);
        long key = status.getId();
        try {
            String serializedTweet = objectMapper.writeValueAsString(tweet);
            ProducerRecord<Long, String> record = new ProducerRecord<>(kafkaTopic, key, serializedTweet);
            // send data
            producer.send(record);
        }
        catch (JsonProcessingException jpe) {
            logger.error("Couldn't serialize " + tweet, jpe);
        }
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        // NOOP
    }

    @Override
    public void onTrackLimitationNotice(int i) {
        // NOOP
    }

    @Override
    public void onScrubGeo(long l, long l1) {
        // NOOP
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
        // NOOP
    }

    @Override
    public void onException(Exception e) {
        logger.error("Status Listener got exception!", e);
    }
}
