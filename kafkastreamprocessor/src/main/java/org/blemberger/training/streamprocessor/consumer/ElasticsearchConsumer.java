package org.blemberger.training.streamprocessor.consumer;

import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ElasticsearchConsumer {

    private static final String BOOTSTRAP_SERVERS = System.getProperty("bootstrap.servers", "localhost:9092");
    private static final String ELASTICSEARCH_SERVER = System.getProperty("es.server", "localhost");
    private static final String ELASTICSEARCH_PORT = System.getProperty("es.port", "9200");
    private static final String GROUP_ID = "elasticsearch-consumer";
    private static final String KAFKA_TOPIC = "twitter-trump-stream";
    private static final String ES_INDEX = "twitter_trump";

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchConsumer.class);

    public static void main(String[] args) throws IOException {

        RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(new HttpHost(ELASTICSEARCH_SERVER, Integer.parseInt(ELASTICSEARCH_PORT), "http")));

        // create consumer properties
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // create consumer
        KafkaConsumer<Long, String> consumer = new KafkaConsumer<>(properties);

        // subscribe to a topic
        consumer.subscribe(Collections.singleton(KAFKA_TOPIC));

        // poll for data
        while(true) {
            ConsumerRecords<Long, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<Long, String> record: records) {
                logger.info("Key: " + record.key() + ", Value: " + record.value());
                logger.info("Partition: " + record.partition() + ", Offset: " + record.offset());
                IndexRequest request = new IndexRequest(ES_INDEX);
                request.source(record.value(), XContentType.JSON);
                try {
                    IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
                    logger.info("ES Status: " + indexResponse.status().getStatus());
                } catch (ElasticsearchException ee) {
                    logger.error("ES Error", ee);
                }
            }
        }
    }
}
