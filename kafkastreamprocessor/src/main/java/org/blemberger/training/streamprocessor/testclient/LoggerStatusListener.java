package org.blemberger.training.streamprocessor.testclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

public class LoggerStatusListener implements StatusListener {

    private static final Logger logger = LoggerFactory.getLogger(LoggerStatusListener.class);

    @Override
    public void onStatus(Status status) {
        logger.info("Status tweet on {} from: {}\nText: {}", status.getCreatedAt(), status.getUser().getScreenName(), status.getText());
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        // NOOP
    }

    @Override
    public void onTrackLimitationNotice(int i) {
        // NOOP
    }

    @Override
    public void onScrubGeo(long l, long l1) {
        // NOOP
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
        // NOOP
    }

    @Override
    public void onException(Exception e) {
        logger.error("Status Listener got exception!", e);
    }
}
