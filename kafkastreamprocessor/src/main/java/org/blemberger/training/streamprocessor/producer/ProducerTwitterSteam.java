package org.blemberger.training.streamprocessor.producer;

import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.twitter.hbc.twitter4j.Twitter4jStatusClient;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StatusListener;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;

public class ProducerTwitterSteam {

    private static final String BOOTSTRAP_SERVERS = System.getProperty("bootstrap.servers", "localhost:9092");
    private static final String KAFKA_TOPIC = "twitter-trump-stream";
    private static final List<String> TWITTER_TERMS = Arrays.asList("trump", "impeachment");
    private static final Logger logger = LoggerFactory.getLogger(ProducerTwitterSteam.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        if (args.length != 4) {
            System.err.println("Need 4 arguments: consumerKey, consumerSecret, accessToken, accessTokenSecret");
            System.exit(-1);
        }

        new ProducerTwitterSteam().execute(args[0], args[1], args[2], args[3]);
    }

    private KafkaProducer<Long, String> buildKafkaProducer() {
        // create producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        return new KafkaProducer<>(properties);
    }

    private Client buildHosebirdClient(Authentication auth, BlockingQueue msgQueue) {

        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        // set up some track terms
        hosebirdEndpoint.trackTerms(TWITTER_TERMS);

        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(auth)
                .endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue));

        return builder.build();
    }

    private Twitter4jStatusClient buildTwitterClient(Client hbc, BlockingQueue<String> msgQueue, KafkaProducer<Long, String> producer) {
        // client is our Client object
        // msgQueue is our BlockingQueue<String> of messages that the handlers will receive from
        // listeners is a List<StatusListener> of the t4j StatusListeners
        // executorService
        List<StatusListener> listeners = Collections.singletonList(new KafkaProducerStatusListener(producer, KAFKA_TOPIC));
        return new Twitter4jStatusClient(hbc, msgQueue, listeners, Executors.newSingleThreadExecutor());
    }

    private void execute(final String consumerKey, final String consumerSecret, final String accessToken, final String accessTokenSecret) {

        CountDownLatch latch = new CountDownLatch(1);

        Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, accessToken, accessTokenSecret);

        // Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
        KafkaProducer<Long, String> producer = buildKafkaProducer();

        Twitter4jStatusClient twitterClient = buildTwitterClient(buildHosebirdClient(hosebirdAuth, msgQueue), msgQueue, producer);

        // Create a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                logger.info("Caught shutdown hook");
                latch.countDown();
                twitterClient.stop();
                producer.close(); // flush & close producer
                logger.info("Application has exited");
            }
        }));

        // Attempts to establish a connection.
        twitterClient.connect();
        twitterClient.process();

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            logger.error("Application got interrupted", e);
        } finally {
            logger.info("Application is closing");
        }
    }
}